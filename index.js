// Setup the dependencies 
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//This allows us to use all the routes defines in "userRoute.js"
const userRoute = require ("./routes/userRoute");
//This allows us to use all the routes defines in "courseRoute.js"
const courseRoute = require ("./routes/courseRoute");


// Server setup
const app = express();

//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
app.use("/users", userRoute);
// Allows all the user routes created in "courseRoute.js" file to use "/courses" as route (resources)
app.use("/courses", courseRoute);

// Database connection
mongoose.connect("mongodb+srv://laronjhon:admin123@zuitt-bootcamp.tyql6ow.mongodb.net/courseBookingAPI?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}	
);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`));

//Server listening
// Will used the defined port number for the application whoever environment variable is available or used port 4000 if none is defined
app.listen(process.env.PORT || 4000, console.log(`Now connected to port ${process.env.PORT || 4000}`));